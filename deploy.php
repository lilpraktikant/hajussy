<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'Neko');
set('remote_user', 'virt101576'); //virt...
set('http_user', 'virt101576');
set('keep_releases', 2);

// Hosts
host('tak20riisik.itmajakas.ee')
    ->setHostname('tak20riisik.itmajakas.ee')
    ->set('http_user', 'virt101576')
    ->set('deploy_path', '~/domeenid/www.tak20riisik.itmajakas.ee/hautatudhakklihamaasikamoosigasinepikastmes')
    ->set('branch', 'master');

// Tasks
set('repository', 'git@gitlab.com:LilPraktikant/hajussy.git');
//Restart opcache
task('opcache:clear', function () {
    run('killall php81-cgi || true');
})->desc('Clear opcache');

task('build:node', function () {
    cd('{{release_path}}');
    run('npm i');
    run('npx vite build');
    run('rm -rf node_modules');
});
task('deploy', [
    'deploy:prepare',
    'deploy:vendors',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'build:node',
    'deploy:publish',
    'opcache:clear',
    'artisan:cache:clear'
]);
after('deploy:failed', 'deploy:unlock');
