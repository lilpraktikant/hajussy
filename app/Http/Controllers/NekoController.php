<?php

namespace App\Http\Controllers;

use App\Models\Neko;
use Illuminate\Http\Request;
use Inertia\Inertia;

class NekoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $neko = Neko::all();
        return Inertia::render('neko', [
            'neko' => $neko
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Addneko');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:655',
            'image' => 'required',
            'cuteness' => 'required',
            'rating' => 'required',
        ]);
        Neko::create([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $request->image,
            'cuteness' => $request->cuteness,
            'rating' => $request->rating,
        ]);
        return redirect(route('neko.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(neko $neko)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(neko $neko)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, neko $neko)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(neko $neko)
    {
        //
    }
}
